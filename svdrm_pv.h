#ifndef SVDRM_PV__H
#define SVDRM_PV__H

#include "svdrm_drv.h"
int32_t svdrm_init_pv_display(struct svdrm_device *device);
int32_t svdrm_reconnect_provider(struct pv_display_provider *provider);
void svdrm_teardown_provider(void);

void svdrm_destroy_display(uint32_t key);
void svdrm_destroy_displays(void);

int32_t svdrm_reconnect_display(struct pv_display *display, struct dh_add_display *request);

void svdrm_host_display_changed(struct pv_display_provider *provider,
                        struct dh_display_info *displays, uint32_t num_displays);
void svdrm_add_display_request(struct pv_display_provider *provider, struct dh_add_display *request);
#endif // SVDRM_PV__H
