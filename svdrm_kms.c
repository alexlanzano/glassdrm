//
// SVdrm: SecureView's custom DRM driver
//
// Copyright (C) 2016 Assured Information Security, Inc. All rights reserved.
// Author: Maurice Gale        <galem@ainfosec.com>
// Author: Brendan Kerrigan    <kerriganb@ainfosec.com>
//

#include "svdrm_drv.h"
#include <linux/delay.h>
#include <linux/workqueue.h>
#include "svdrm_pv.h"

static const uint32_t
svdrm_primary_plane_formats[] = {
    DRM_FORMAT_XRGB8888,
    DRM_FORMAT_ARGB8888,
};

static const uint32_t
svdrm_cursor_plane_formats[] = {
    DRM_FORMAT_ARGB8888,
};

static const struct drm_plane_funcs
svdrm_plane_funcs =
{
    .update_plane = drm_atomic_helper_update_plane,
    .disable_plane = drm_atomic_helper_disable_plane,
    .destroy = drm_plane_cleanup,
    .reset = drm_atomic_helper_plane_reset,
    #if (RHEL_RELEASE_CODE != 0 && RHEL_RELEASE_CODE < RHEL_RELEASE_VERSION(7,5)) || \
        (RHEL_RELEASE_CODE == 0 && LINUX_VERSION_CODE < KERNEL_VERSION(4, 14, 0))
        .set_property = drm_atomic_helper_plane_set_property,
    #endif
    .atomic_duplicate_state = drm_atomic_helper_plane_duplicate_state,
    .atomic_destroy_state = drm_atomic_helper_plane_destroy_state,
};

static void
svdrm_crtc_dpms(struct drm_crtc *crtc, int mode)
{
    __SVDRM_TRACE__;

	switch (mode) {
	case DRM_MODE_DPMS_ON:
        svdrm_debug("DRM_MODE_DPMS_ON\n");
	case DRM_MODE_DPMS_STANDBY:
        svdrm_debug("DRM_MODE_DPMS_STANDBY\n");
	case DRM_MODE_DPMS_SUSPEND:
        svdrm_debug("DRM_MODE_DPMS_SUSPEND\n");
	case DRM_MODE_DPMS_OFF:
        svdrm_debug("DRM_MODE_DPMS_OFF\n");
	default:
		return;
	}
}

// Validate a mode. mode is the mode that the userspace
// requested. adjusted mode is the mode that the encoders need to be fed with
static bool
svdrm_crtc_mode_fixup(struct drm_crtc *crtc,
                      const struct drm_display_mode *mode,
                      struct drm_display_mode *adjusted_mode)
{
    __SVDRM_TRACE__;

    drm_mode_debug_printmodeline(mode);
    drm_mode_debug_printmodeline(adjusted_mode);

    return true;
}

static int32_t
svdrm_crtc_mode_set(struct drm_crtc *crtc, struct drm_display_mode *mode,
                    struct drm_display_mode *adjusted_mode,
                    int x, int y,struct drm_framebuffer *old_fb)
{
    struct svdrm_kms_connector_group *connector_group = NULL;
    struct svdrm_kms_mapping *mapping = NULL;
    struct pv_display *display = NULL;
    int32_t rc = 0;

    __SVDRM_TRACE__;

    // Validate Parameters
    if (!crtc || !mode || !adjusted_mode) {
      __SVDRM_TRACE__;

        return -EINVAL;
    }

    svdrm_debug("CRTC ID: %d\n", crtc->base.id);

    // If no framebuffer, exit
    if (crtc->primary->fb == NULL) {
      __SVDRM_TRACE__;
        svdrm_error("No primary fb detected\n");
        return 0;
    }

    // Obtain the svdrm device from the crtc. This will allow us to
    // get the display information
    connector_group = container_of(crtc, struct svdrm_kms_connector_group, crtc);
    mapping = container_of(connector_group, struct svdrm_kms_mapping, connector_group);

    if (!mapping || !mapping->display) {
        svdrm_debug("No mapping or display object detected\n");
        return -ENOENT;
    }

    if (mapping->connected == false) {
        return -EINVAL;
    }

    // Setup up a shortcut for the display, thats associated with the crtc
    display = mapping->display;

    rc = display->change_resolution(display, mapping->display_hint.width, mapping->display_hint.height,
                                    pixels_to_bytes(mapping->display_hint.width));
    if(rc) {
        svdrm_error("Failed to change resolution for display: %d", display->key);
    }

    display->invalidate_region(display, 0, 0, display->width, display->height);
    display->set_cursor_visibility(display, 1);

    // If for some resaon the change failed, bail out
    return rc;
}

static void
svdrm_crtc_prepare(struct drm_crtc *crtc)
{
    __SVDRM_TRACE__;

    return;
}

static void
svdrm_crtc_commit(struct drm_crtc *crtc)
{
    __SVDRM_TRACE__;



    return;
}

int32_t
svdrm_crtc_cursor_set2(struct drm_crtc *crtc, struct drm_file *file_priv,
                       uint32_t handle, uint32_t width, uint32_t height,
                       int32_t hot_x, int32_t hot_y)
{
    struct svdrm_kms_connector_group *connector_group = NULL;
    struct svdrm_kms_mapping *mapping = NULL;
    struct svdrm_device *device =  NULL;
    struct pv_display *display = NULL;
    struct drm_gem_object *gem = NULL;
    struct svdrm_gem_object *svgem = NULL;
    int32_t rc = 0;

    __SVDRM_TRACE__;

    // Validate Parameters
    if (!crtc || !file_priv || handle < 0 || width != CURSOR_WIDTH ||
        height != CURSOR_HEIGHT) {

        return -EINVAL;
    }

    // Get a reference to the mapping information, svdevice and display
    connector_group = container_of(crtc, struct svdrm_kms_connector_group, crtc);
    mapping = container_of(connector_group, struct svdrm_kms_mapping, connector_group);

    if (!mapping) {
        return -EINVAL;
    }

    device = mapping->dev;

    if (!device) {
        return -EINVAL;
    }

    display = mapping->display;

    // Ensure that we have a display and device before moving on
    if(!display) {
        return -EINVAL;
    }

    // If the handle is zero, hide the cursor
    if (!handle) {
        if (!display->set_cursor_visibility) {
            return 0;
        }

        return display->set_cursor_visibility(display, 0);
    }

    // Get a reference to the gem object associated with the handle
    // then obtain a svgem private object
    gem = svdrm_gem_object_lookup(file_priv->minor->dev, file_priv, handle);
    if (!gem) {
        return -EINVAL;
    }

    // If we have a cursor object get framebuffer information
    svgem = to_svdrm_gem_object(gem);
    if(svgem && svgem->type == SVDRM_CURSOR && svgem->image) {
        // Load cursor ordinarilly
        rc = display->load_cursor_image(display, svgem->image,
                                        64, 64);

        // If for some reason we are unable to load the image, error out
        if (rc) {
            return rc;
        }

        // Invalidate region to force a redraw
        display->invalidate_region(display, 0, 0, display->width, display->height);

        // Set the hot spot for the pv cursor associated with the display
        display->set_cursor_hotspot(display, hot_x, hot_y);

        // Make the cursor visible
        rc = display->set_cursor_visibility(display, 1);
    }

    return rc;
}

int32_t
svdrm_crtc_cursor_move(struct drm_crtc *crtc, int x, int y)
{
    struct svdrm_kms_connector_group *connector_group = NULL;
    struct svdrm_kms_mapping *mapping = NULL;
    struct pv_display *display = NULL;

    __SVDRM_TRACE__;
    
    // Validate Parameters
    if (!crtc) {
        return -ENOENT;
    }

    // Obtain the svdrm device from the crtc. This will allow us to get
    // the display information
    connector_group = container_of(crtc, struct svdrm_kms_connector_group, crtc);
    mapping = container_of(connector_group, struct svdrm_kms_mapping, connector_group);

    if (!mapping || !mapping->dev) {
        return -EINVAL;
    }

    // Create a shortcut to the display object
    display = mapping->display;

    if (!display) {
        return -EINVAL;
    }

    // Make the cursor visible
    display->set_cursor_visibility(display, 1);

    display->move_cursor(display, x, y);

    return 0;
}

#if (RHEL_RELEASE_CODE != 0 && RHEL_RELEASE_CODE >= RHEL_RELEASE_VERSION(7,5)) || \
    (RHEL_RELEASE_CODE == 0 && LINUX_VERSION_CODE >= KERNEL_VERSION(4, 12, 0))
int32_t
svdrm_crtc_helper_set_config(struct drm_mode_set *set, struct drm_modeset_acquire_ctx *ctx)
{
    __SVDRM_TRACE__;

    if (!set) {
        return -ENOENT;
    }

    if (!set->crtc) {
        return 0;
    }
    if (set->fb) {
        return drm_crtc_helper_set_config(set, ctx);
    }

    return 0;
}
#else
int32_t
svdrm_crtc_helper_set_config(struct drm_mode_set *set)
{
    __SVDRM_TRACE__;

    if (!set) {
        return -ENOENT;
    }

    if (!set->crtc) {
        return 0;
    }
    if (set->fb) {
        return drm_crtc_helper_set_config(set);
    }

    return 0;
}
#endif

static const struct drm_crtc_helper_funcs
svdrm_crtc_helper_funcs = {
    .dpms = svdrm_crtc_dpms,
    .prepare = svdrm_crtc_prepare,
	.mode_fixup = svdrm_crtc_mode_fixup,
    .mode_set = svdrm_crtc_mode_set,
    .commit = svdrm_crtc_commit,
};

const struct drm_crtc_funcs
svdrm_crtc_funcs = {
    .set_config = svdrm_crtc_helper_set_config,
    .destroy = drm_crtc_cleanup,
    .reset = drm_atomic_helper_crtc_reset,
    .cursor_set2 = svdrm_crtc_cursor_set2,
    .cursor_move = svdrm_crtc_cursor_move,
    .atomic_duplicate_state = drm_atomic_helper_crtc_duplicate_state,
    .atomic_destroy_state = drm_atomic_helper_crtc_destroy_state,
};

static void
svdrm_encoder_dpms(struct drm_encoder *encoder, int mode)
{
    __SVDRM_TRACE__;

    return;
}

static bool
svdrm_encoder_mode_fixup(struct drm_encoder *encoder,
                         const struct drm_display_mode *mode,
                         struct drm_display_mode *adjusted_mode)
{
    __SVDRM_TRACE__;


    drm_mode_debug_printmodeline(mode);
    drm_mode_debug_printmodeline(adjusted_mode);

    return true;
}

static void
svdrm_encoder_prepare(struct drm_encoder *encoder)
{
    __SVDRM_TRACE__;

    return;
}

static void
svdrm_encoder_commit(struct drm_encoder *encoder)
{
    __SVDRM_TRACE__;

    return;
}

static void
svdrm_encoder_mode_set(struct drm_encoder *encoder,
                       struct drm_display_mode *mode,
                       struct drm_display_mode *adjusted_mode)
{
    __SVDRM_TRACE__;

    drm_mode_debug_printmodeline(mode);
    drm_mode_debug_printmodeline(adjusted_mode);

    return;
}

//    .dpms = svdrm_encoder_dpms,
struct drm_encoder_helper_funcs
svdrm_encoder_helper_funcs = {
    .dpms       = svdrm_encoder_dpms,
    .mode_fixup = svdrm_encoder_mode_fixup,
    .prepare    = svdrm_encoder_prepare,
    .commit     = svdrm_encoder_commit,
    .mode_set   = svdrm_encoder_mode_set,
};

void svdrm_encorder_cleanup(struct drm_encoder *encoder)
{
    if(!encoder) {
      return;
    }

    drm_encoder_cleanup(encoder);
}

struct drm_encoder_funcs
svdrm_encoder_funcs = {
    .destroy = drm_encoder_cleanup
};

static
void svdrm_guess_mode_timing(struct drm_display_mode *mode)
{
  mode->hsync_start = mode->hdisplay + 50;
  mode->hsync_end = mode->hsync_start + 50;
  mode->htotal = mode->hsync_end + 50;

  mode->vsync_start = mode->vdisplay + 50;
  mode->vsync_end = mode->vsync_start + 50;
  mode->vtotal = mode->vsync_end + 50;

  mode->clock = (u32)mode->htotal * (u32)mode->vtotal / 100 * 6;
  mode->vrefresh = drm_mode_vrefresh(mode);
}

// Fills in all modes that are available for the sink into the
// connector->probed_modes list. It should also update the EDID by calling
// drm_mode_connector_update_edid_property
static int32_t
svdrm_connector_get_modes(struct drm_connector *connector)
{
    struct svdrm_kms_connector_group *connector_group = NULL;
    struct svdrm_kms_mapping *mapping = NULL;
    struct pv_display *display = NULL;
    struct drm_display_mode *mode = NULL;
    struct drm_display_mode prefmode = { DRM_MODE("preferred",
        DRM_MODE_TYPE_DRIVER | DRM_MODE_TYPE_PREFERRED,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        DRM_MODE_FLAG_NHSYNC | DRM_MODE_FLAG_PVSYNC)
      };
    int32_t count = 0;

    __SVDRM_TRACE__;

    // Validate parameters
    if (!connector) {
        svdrm_debug("Null connector\n");
        return -EINVAL;
    }

    // Obtain the svdrm device from the connector. This will allow us to get
    // the display information
    connector_group = container_of(connector, struct svdrm_kms_connector_group, connector);
    mapping = container_of(connector_group, struct svdrm_kms_mapping, connector_group);

    if (!mapping || !mapping->dev) {
        return -EINVAL;
    }

    // Setup a shortcut for the display
    display = mapping->display;

    if (!display) {
        svdrm_debug("No display detected in get_modes, creating a default mode\n");
        count = drm_add_modes_noedid(&connector_group->connector,
                                     MODE_DEFAULT_WIDTH,
                                     MODE_DEFAULT_HEIGHT);
        drm_set_preferred_mode(connector, MODE_DEFAULT_WIDTH,
                               MODE_DEFAULT_HEIGHT);
        return count + 1;
    }

    // Add modes for connectors without edids. This adds the specified modes
    // to the connectors mode list
    // count = drm_add_modes_noedid(&connector_group->connector, display->width,
    //                              display->height);

    // Create the custom mode
    mode = drm_mode_duplicate(mapping->dev->dev, &prefmode);
    if (!mode)
          return -EINVAL;

    svdrm_debug("Creating a mode for %dx%d\n", mapping->display_hint.width, mapping->display_hint.height);

    mode->hdisplay = mapping->display_hint.width;
    mode->vdisplay = mapping->display_hint.height;

    svdrm_guess_mode_timing(mode);

    // Add Mode to mode list
    drm_mode_probed_add(connector, mode);

    // Increment the total number of available modes
    count++;

    // Sets the preferred mode for the connector.
    drm_set_preferred_mode(connector, mapping->display_hint.width, mapping->display_hint.height);

    return count;
}

// Validates a mode for a connector
static enum drm_mode_status
svdrm_connector_mode_valid(struct drm_connector *connector,
                           struct drm_display_mode *mode)
{
    struct svdrm_kms_connector_group *connector_group = NULL;
    struct svdrm_kms_mapping *mapping = NULL;
    struct pv_display *display = NULL;

    __SVDRM_TRACE__;

    // Validate parameters
    if (!connector || !mode)
        return MODE_BAD;

    // Obtain the svdrm device from the connector. This will allow us to
    // get the display information
    connector_group = container_of(connector, struct svdrm_kms_connector_group, connector);
    mapping = container_of(connector_group, struct svdrm_kms_mapping, connector_group);

    if (!mapping || !mapping->dev) {
        svdrm_debug("No mapping in mode_valid\n");
        return MODE_BAD;
    }

    // Setup a shortcut for the display
    display = mapping->display;

    if (!display) {
        svdrm_debug("No display in mode_valid\n");
        return MODE_BAD;
    }

    if(mode->hdisplay == 0 || mode->vdisplay == 0) {
        svdrm_debug("Mode is set to zero. Bad mode\n");
        return MODE_BAD;
    }

    if (mode->hdisplay == mapping->display_hint.width && mode->vdisplay == mapping->display_hint.height) {
        svdrm_debug("Found a mode that matches the hint's mode\n");
        return MODE_OK;
    }

    return MODE_BAD;
}

// Find the best encoder for a given connector
static struct drm_encoder*
svdrm_connector_best_encoder(struct drm_connector *connector)
{
    struct svdrm_kms_connector_group *connector_group = NULL;
    struct drm_display_mode *mode = NULL;
    __SVDRM_TRACE__;

    if (!connector)
        return ERR_PTR(-EINVAL);

    list_for_each_entry(mode, &connector->modes, head) {
      drm_mode_debug_printmodeline(mode);
    }

    // Obtain the svdrm device from the connector. This will allow us to
    // get the display information
    connector_group = container_of(connector, struct svdrm_kms_connector_group, connector);

    return &connector_group->encoder;
}

struct drm_connector_helper_funcs
svdrm_connector_helper_funcs = {
    .get_modes = svdrm_connector_get_modes,
    .mode_valid = svdrm_connector_mode_valid,
    .best_encoder = svdrm_connector_best_encoder,
};

// Check to see if anything is attached to the connector
// force should be set to false for polling and true when checking for the
// connector with user request
static enum drm_connector_status
svdrm_connector_detect(struct drm_connector *connector, bool force)
{
    struct svdrm_kms_connector_group *connector_group = NULL;
    struct svdrm_kms_mapping *mapping = NULL;

    __SVDRM_TRACE__;

    if (!connector) {
        svdrm_debug("No connector. Setting connector status to unknown\n");
        return connector_status_unknown;
    }

    connector_group = container_of(connector, struct svdrm_kms_connector_group, connector);
    mapping = container_of(connector_group, struct svdrm_kms_mapping, connector_group);

    if (!mapping || !mapping->dev) {
        svdrm_debug("No mapping. Setting connector status to unknown\n");
        return connector_status_unknown;
    }

    svdrm_debug("Looking for connector for key 0x%08xd\n", mapping->key);

    if (mapping->display == NULL) {
        svdrm_debug("Display for key %d is NULL so marking connector as disconnected\n", mapping->key);
        return connector_status_disconnected;
    }

    if (connector_group->connector.status == connector_status_connected) {
        svdrm_debug("Connector is connected\n");
    } else {
        svdrm_debug("Connector is disconnected\n");
    }

    return connector_group->connector.status;
}

static void svdrm_connector_destroy(struct drm_connector *connector)
{
    drm_connector_unregister(connector);
    drm_connector_cleanup(connector);
}

static const struct drm_connector_funcs
svdrm_connector_funcs = {
    .dpms = drm_helper_connector_dpms,
    .detect = svdrm_connector_detect,
    .fill_modes = drm_helper_probe_single_connector_modes,
    .destroy = svdrm_connector_destroy,
    .reset = drm_atomic_helper_connector_reset,
    .atomic_destroy_state = drm_atomic_helper_connector_destroy_state,
    .atomic_duplicate_state = drm_atomic_helper_connector_duplicate_state,
};

int32_t
svdrm_init_crtc_state(struct drm_crtc *crtc)
{
    struct drm_crtc_state *state = NULL;

    __SVDRM_TRACE__;

    // Verify parameters
    if (!crtc) {
        return -ENOMEM;
    }

    // If a state object doesn't yet exist create one.
    if (!crtc->state) {
        // Allocate memory for the state object of the crtc
        state = kzalloc(sizeof(*state), GFP_KERNEL);

        // If we are not able to allocate memory, error out
        if (!state)
            return -ENOMEM;

        // Make sure the state is pointing to the correct crtc
        // and setup back pointer for the crtc embedded state object
        state->crtc = crtc;
        state->enable = false;
        state->plane_mask = 1 << drm_crtc_index(crtc);
        crtc->state = state;
    }

    return 0;
}

int32_t
svdrm_init_plane_state(struct svdrm_kms_connector_group *connector_group,
                       struct drm_plane *plane)
{
    struct drm_plane_state *state = NULL;

    __SVDRM_TRACE__;

    if (!connector_group || !plane) {
        return -EINVAL;
    }

    // Create initial state if there is no state associated with the object
    if (!plane->state) {
        // Get some memory for our custom plane state object
        state = kzalloc(sizeof(*state), GFP_KERNEL);

        // If we can not get memory, then error out
        if (!state)
            return -ENOMEM;

        // Setup some initializations. Make sure everything is pointing to
        // where it is suppose to be pointing.
        state->plane = plane;
        state->crtc = &connector_group->crtc;
        plane->state = state;
    }

    return 0;
}

int32_t
svdrm_reinitialize_states(struct svdrm_kms_connector_group *connector_group)
{
    if (!connector_group) {
        return -EINVAL;
    }

    __SVDRM_TRACE__;
    if (connector_group->crtc.state) {
        svdrm_debug("Removing old crtc state");
        kfree(connector_group->crtc.state);
    }

    if (connector_group->primary.state) {
        svdrm_debug("Removing old plane state\n");
        kfree(connector_group->primary.state);
    }

    // Initialize state informatation
    if (svdrm_init_crtc_state(&connector_group->crtc) ||
        svdrm_init_plane_state(connector_group, &connector_group->primary)) {
        return -EINVAL;
    }

    return 0;
}

int32_t
svdrm_init_plane(struct drm_device *device,
                 struct svdrm_kms_connector_group *connector_group,
                 enum drm_plane_type type)
{
    struct drm_plane *plane = NULL;
    const uint32_t *formats;
    uint32_t format_count = 0;
    int32_t rc = 0;
    uint32_t possible_crtcs;

    __SVDRM_TRACE__;

    if (!device || !connector_group) {
        return -EINVAL;
    }

    // The type of plane to create determine its parameters for the
    // initialization function
    switch (type)
    {
        case DRM_PLANE_TYPE_OVERLAY:

        case DRM_PLANE_TYPE_PRIMARY:
            format_count = ARRAY_SIZE(svdrm_primary_plane_formats);
            formats = svdrm_primary_plane_formats;
            connector_group->crtc.primary = plane = &connector_group->primary;
            break;

        case DRM_PLANE_TYPE_CURSOR:
            format_count = ARRAY_SIZE(svdrm_cursor_plane_formats);
            formats = svdrm_cursor_plane_formats;
            connector_group->crtc.cursor = plane = &connector_group->cursor;
            break;

        default:
            svdrm_error("%s - Invalid plane type detected\n", __FUNCTION__);
            return -EINVAL;
    }

    possible_crtcs = 1 << connector_group->index;

    #if (RHEL_RELEASE_CODE != 0 && RHEL_RELEASE_CODE >= RHEL_RELEASE_VERSION(7,5)) || \
        (RHEL_RELEASE_CODE == 0 && LINUX_VERSION_CODE >= KERNEL_VERSION(4, 14, 0))
        rc = drm_universal_plane_init(device, plane, possible_crtcs,
                                      &svdrm_plane_funcs, formats,
                                      format_count, NULL, type, NULL);

    #elif (RHEL_RELEASE_CODE != 0 && RHEL_RELEASE_CODE == RHEL_RELEASE_VERSION(7,4)) || \
          (RHEL_RELEASE_CODE == 0 && LINUX_VERSION_CODE > KERNEL_VERSION(4, 5, 0))
        rc = drm_universal_plane_init(device, plane, possible_crtcs,
                                      &svdrm_plane_funcs, formats,
                                      format_count, type, NULL);
    #else
        rc = drm_universal_plane_init(device, plane, possible_crtcs,
                                      &svdrm_plane_funcs, formats,
                                      format_count, type);
    #endif

    // If we are unable to initialize the plane, error out
    if (rc) {
        return rc;
    }

    return 0;
}

int32_t
svdrm_init_crtc(struct drm_device *device, struct svdrm_kms_connector_group *connector_group)
{
    int32_t rc = 0;

    __SVDRM_TRACE__;

    if (!device || !connector_group) {
        return -EINVAL;
    }
    __SVDRM_TRACE__;
    // Create and initialize a primary plane
    rc = svdrm_init_plane(device, connector_group, DRM_PLANE_TYPE_PRIMARY);
    if (rc) {
        return rc;
    }
    __SVDRM_TRACE__;
    // Initialize a CRTC object with a default provided primary plane
    // and no cursor plane

    #if (RHEL_RELEASE_CODE != 0 && RHEL_RELEASE_CODE >= RHEL_RELEASE_VERSION(7,4)) || \
        (RHEL_RELEASE_CODE == 0 && LINUX_VERSION_CODE >= KERNEL_VERSION(4, 5, 0))
        rc = drm_crtc_init_with_planes(device, &connector_group->crtc,
                                       &connector_group->primary, NULL, &svdrm_crtc_funcs, NULL);
    #else
        rc = drm_crtc_init_with_planes(device, &connector_group->crtc,
                                       &connector_group->primary, NULL, &svdrm_crtc_funcs);
    #endif

    __SVDRM_TRACE__;

    // If we were not able to initialize the crtc then fail out
    if (rc) {
        return rc;
    }

    __SVDRM_TRACE__;

    // Initialize state informatation
    if (svdrm_init_crtc_state(&connector_group->crtc) ||
        svdrm_init_plane_state(connector_group, &connector_group->primary)) {
        return -EINVAL;
    }
    __SVDRM_TRACE__;
    // Ensure that the plane back pointers to its crtcs is setup correctly
    connector_group->primary.crtc = &connector_group->crtc;

    // Set up the helper function table to the crtc
    drm_crtc_helper_add(&connector_group->crtc, &svdrm_crtc_helper_funcs);
    drm_mode_crtc_set_gamma_size(&connector_group->crtc, 256);
    connector_group->crtc.enabled = false;

    return rc;
}

static int32_t
svdrm_init_encoder(struct drm_device *device, struct svdrm_kms_connector_group *connector_group)
{
    int32_t rc = 0;

    __SVDRM_TRACE__;

    if (!connector_group || !device) {
        return -EINVAL;
    }

    // Configure some options for the encoder. Make it compatable with all
    // crtcs. No possible cloning
    connector_group->encoder.possible_clones = 0;
    connector_group->encoder.possible_crtcs = 1 << connector_group->index;

    // Perform that actual initialization of the encoder
    #if (RHEL_RELEASE_CODE != 0 && RHEL_RELEASE_CODE >= RHEL_RELEASE_VERSION(7,4)) || \
        (RHEL_RELEASE_CODE == 0 && LINUX_VERSION_CODE >= KERNEL_VERSION(4, 5, 0))
        rc = drm_encoder_init(device, &connector_group->encoder,
                              &svdrm_encoder_funcs, DRM_MODE_ENCODER_VIRTUAL, NULL);
    #else
        rc = drm_encoder_init(device, &connector_group->encoder,
                              &svdrm_encoder_funcs, DRM_MODE_ENCODER_VIRTUAL);
    #endif

    // If we are not bale to initialize the encoder fail out.
    if (rc) {
        return rc;
    }

    // Sets up the helper function table for the encoder
    drm_encoder_helper_add(&connector_group->encoder, &svdrm_encoder_helper_funcs);

    return rc;
}

int32_t
svdrm_init_connector(struct drm_device *device, struct svdrm_kms_connector_group *connector_group)
{
    int32_t rc = 0;
    struct drm_connector_state *state = NULL;

    __SVDRM_TRACE__;

    if(!device || !connector_group) {
      return -EINVAL;
    }

    // Initialize a preallocated connector
    rc = drm_connector_init(device, &connector_group->connector,
                            &svdrm_connector_funcs, DRM_MODE_CONNECTOR_VIRTUAL);

    // If for some reason the connector was unable to be initialized,
    // error out since we need a connector
    if (unlikely(rc))
        return rc;

    if (!connector_group->connector.state) {
        // Allocate memory for the state object of the crtc
        state = kzalloc(sizeof(*state), GFP_KERNEL);
	if (!state) {
	    svdrm_error("Unable to allocate memory for connector_state");
	    return -ENOMEM;
	}
	state->crtc = &connector_group->crtc;
	state->best_encoder = &connector_group->encoder;
    state->connector = &connector_group->connector;
    }

    connector_group->connector.state = state;

    // Sets up the helper function table for the connector
    drm_connector_helper_add(&connector_group->connector,
                             &svdrm_connector_helper_funcs);

    return rc;
}

int32_t
svdrm_kms_init_connector_group(struct drm_device *device, struct svdrm_kms_connector_group *connector_group)
{
    int32_t rc = 0;

    __SVDRM_TRACE__;

    rc = svdrm_init_crtc(device, connector_group);
    if(rc) {
        return rc;
    }

    rc = svdrm_init_encoder(device, connector_group);
    if(rc) {
        return rc;
    }

    rc = svdrm_init_connector(device, connector_group);
    if(rc) {
        return rc;
    }

    // Attach encoder to connector
#if (RHEL_RELEASE_CODE != 0 && RHEL_RELEASE_CODE <= RHEL_RELEASE_VERSION(7,6)) || \
    (RHEL_RELEASE_CODE == 0 && LINUX_VERSION_CODE < KERNEL_VERSION(4, 18, 0))
        rc = drm_mode_connector_attach_encoder(&connector_group->connector,
                                               &connector_group->encoder);
#else
        rc = drm_connector_attach_encoder(&connector_group->connector,
                                               &connector_group->encoder);
#endif
    if(rc) {
        return rc;
    }

    connector_group->connector.encoder = &connector_group->encoder;
    connector_group->encoder.crtc = &connector_group->crtc;

    // Register the connector which makes it usuable in userspace
    rc = drm_connector_register(&connector_group->connector);

    return rc;
}

int32_t
svdrm_kms_init(struct svdrm_device *device)
{
    int32_t index = 0, rc = 0, i = 0;
    struct svdrm_kms_mapping *mapping = NULL;
    __SVDRM_TRACE__;

    // Validate parameters
    if (!device) {
        return -EINVAL;
    }

    // Initializa DRM mode configuration structure
    drm_mode_config_init(device->dev);

    // Configure default Max/Min Values for height and width
    device->dev->mode_config.min_height = MODE_MIN_HEIGHT;
    device->dev->mode_config.min_width = MODE_MIN_WIDTH;
    device->dev->mode_config.max_height = MODE_4K_HEIGHT * MAX_DISPLAYS;
    device->dev->mode_config.max_width = MODE_4K_WIDTH * MAX_DISPLAYS;
    device->dev->mode_config.prefer_shadow = MODE_DEFAULT_SHADOW;
    device->dev->mode_config.preferred_depth = MODE_DEFAULT_DEPTH;
    device->dev->mode_config.funcs = &svdrm_mode_funcs;

    // Iterate over each mappping, and initialize the connector group (crtc->encoder->connector),
    hash_for_each(device->kms_map, i, mapping, hash_node) {
        if (mapping) {
            mapping->connector_group.index = index++;
            rc = svdrm_kms_init_connector_group(device->dev, &mapping->connector_group);
            if (rc) {
                continue;
            }
            mapping->connector_registered = true;
        }
    }

    svdrm_debug("Initialized KMS\n");
    return 0;
}

void
svdrm_print_mapping_information(struct svdrm_kms_connector_group *connector_group)
{
    if (!connector_group) {
        return;
    }

    svdrm_debug("Mapping Information: CRTC[%d]->Encoder[%d]->Connector[%d]\n",
                                                connector_group->crtc.base.id,
                                                connector_group->encoder.base.id,
                                                connector_group->connector.base.id);
}

void
svdrm_kms_cleanup(struct svdrm_kms_connector_group *connector_group)
{
    __SVDRM_TRACE__;

    if (!connector_group) {
        return;
    }

    drm_connector_unregister(&connector_group->connector);
    drm_crtc_cleanup(&connector_group->crtc);
    drm_encoder_cleanup(&connector_group->encoder);
    drm_connector_cleanup(&connector_group->connector);
    /*if (connector_group.primary) {
        drm_plane_cleanup(connector_group->primary);
    }

    if (connector_group->cursor) {
        drm_plane_cleanup(connector_group->cursor);
    }
*/
    return;
}

void
svdrm_pv_remove_display(struct pv_display_provider *provider, struct dh_remove_display *request)
{
    __SVDRM_TRACE__;

    // Verify parameters
    if (!provider || !request) {
        return;
    }

    svdrm_destroy_display(request->key);

    return;
}

void
svdrm_handle_display_error(struct pv_display *display, void* userData)
{
    __SVDRM_TRACE__;

    if (!display) {
        return;
    }

    svdrm_debug("Display error detected for key: %d\n", display->key);
    svdrm_update_display_connection_status(display, false, SVDRM_STATUS_BUSY);
}
